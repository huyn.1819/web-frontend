import {Button, Container, FormControl, InputGroup, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {useAuth} from "../contexts/authContext";
import {useHistory} from "react-router-dom";

function Header(props)
{
    const auth = useAuth()
    const history = useHistory();

    const logout = async (e) => {
        e.preventDefault()
        auth.logout()
        history.push('/login')
    }

    return (
        <Navbar bg="primary" variant="dark">
            <Container>
                <Nav className="me-auto">
                    <Nav.Link href="/users">Home</Nav.Link>
                    <Nav.Link>
                        <InputGroup className="mb-3">
                            <FormControl placeholder="Search..."/>
                            <Button variant="outline-dark" id="button-addon2">Search</Button>
                        </InputGroup>
                    </Nav.Link>
                    <NavDropdown title={auth.userName} id="nav-dropdown">
                        <NavDropdown.Item onClick={ logout }>Log out</NavDropdown.Item>
                    </NavDropdown>
                </Nav>
            </Container>
        </Navbar>
    )

}

export default Header;