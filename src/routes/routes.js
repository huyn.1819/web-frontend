import Login from "../pages/login";
import Dashboard from "../pages/dashboard";

const routes = [
    {
        path: "/dashboard",
        component: Dashboard
    },
    {
        path: '/login',
        component: Login
    },
]

export default routes
