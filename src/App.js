import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { AuthProvider } from "./contexts/authContext";

import Home from "./pages/home";
import routes from "./routes/routes";
import PrivateRoute from './pages/protected';

function App() {
    return (
        <AuthProvider>
            <Router>
                <Switch>
                    {routes.map((route, i) => (
                        <Route key={i} {...route} />
                    ))}
                </Switch>
                <PrivateRoute path='/home' component={Home} />
            </Router>
        </AuthProvider>
    );
}
export default App;
