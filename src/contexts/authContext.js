import React, { useContext, useState } from "react";
import { Login, getUser } from "../services/authService";

const AuthContext = React.createContext(null);

export function AuthProvider ({ children }) {

    const [user, setUser] = useState(false);
    const [userName, setUserName] = useState("")

    const login = async (data) => {
        try {
            let response = await Login(data)
            if (response.data.data.access_token)
            {
                localStorage.setItem("current user", JSON.stringify(response.data.data));
                setUser(true)
                setUserName(response.data.data.user.name)
            }
            console.log("data", response)
        } catch (error) {
            console.log('error', error)
        }
    }

    const logout = async () => {
        if (localStorage.getItem("current user") !== null)
        {
            localStorage.removeItem("current user");
            setUser(false)
            console.log("Logout successfully.")
        } else {
            console.log("You are not login.")
        }

    }

    const getUsers = async () => {
        try {
            let response = await getUser()
            console.log("data", response)
        } catch (error) {
            console.log('error', error)
        }
    }

    const authenticate = {
        user,
        userName,
        login,
        logout,
        getUsers
    }
    return (
        <AuthContext.Provider value={authenticate}>
                {children}
        </AuthContext.Provider>
    );
}

export const useAuth = ()=>{
     return useContext(AuthContext);
}
