import API from "../config/axios";

export async function loginUser(dispatch, dataLogin) {
    try {
        dispatch({ type: 'REQUEST_LOGIN' });
        let response = await API.post('login', dataLogin);
        let data = response.data;
        if (response.data.data.user) {
            dispatch({ type: 'LOGIN_SUCCESS', payload: data });
            localStorage.setItem('currentUser', JSON.stringify(data));
            return data;
        }

        dispatch({ type: 'LOGIN_ERROR', error: data.error[0] });
    } catch (error) {
        dispatch({ type: 'LOGIN_ERROR', error: error });
    }
}

export async function logout(dispatch) {
    dispatch({ type: 'LOGOUT' });
    localStorage.removeItem('currentUser');
    localStorage.removeItem('access_token');
}