import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useAuth } from "../contexts/authContext";

const PrivateRoute = ({ component: Component, ...rest }) => {
    let auth = useAuth();
    console.log(auth);
    return (
        <Route {...rest} render={(props) => (
            auth?.user
                ? <Component {...props} />
                : <Redirect to={{
                    pathname: '/login',
                    state: { from: props.location }
                }} />
        )} />
    )
}
export default PrivateRoute;

// object trong js
//arrow function 