import React, { useState } from 'react';
import '../assets/styles/App.css'
import {useAuth} from "../contexts/authContext";
import {useHistory} from "react-router-dom";
import {Button, Col, Form, Row} from "react-bootstrap";
import Dashboard from "./dashboard";

function Login(props)
{
    const auth = useAuth();
    const history = useHistory();

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const login = async (e) => {
        e.preventDefault()
        const dataLogin = {
            email: email,
            password: password,
        }
        auth.login(dataLogin)
        if (auth.user === true)
        {
            history.push("/home")
        }
    }

    return (
        <div>
            <Dashboard />
            <div className="login">
                <Form.Group as={Row} className="mb-3" controlId="formHorizontalEmail">
                    <Form.Label column sm={2}>Email</Form.Label>
                    <Col sm={10}>
                        <Form.Control
                            className="email"
                            type="email"
                            placeholder="Email"
                            onChange={ event => setEmail(event.target.value) }
                            value={ email }
                        />
                    </Col>
                </Form.Group>

                <Form.Group as={Row} className="mb-3" controlId="formHorizontalPassword">
                    <Form.Label column sm={2}>Password</Form.Label>
                    <Col sm={10}>
                        <Form.Control
                            className="password"
                            type="password"
                            placeholder="Password"
                            onChange={ event => setPassword(event.target.value) }
                            value={ password }
                        />
                    </Col>
                </Form.Group>
                <Form.Group className="mb-3" controlId="formBasicCheckbox">
                    <Form.Check type="checkbox" label="Remember me" />
                </Form.Group>
                <Form.Group as={Row} className="mb-3">
                    <Col sm={{ span: 10, offset: 2 }}>
                        <Button onClick={ login } type="submit" className="submit">Login</Button>
                    </Col>
                </Form.Group>
            </div>
        </div>
    );
}

export default Login;


