import React from "react";
import { useAuth } from "../contexts/authContext";
import Header from "../layouts/header";

function Home(props)
{
    const auth = useAuth()

    const getUser = async (e) => {
        e.preventDefault()
        auth.getUsers()
    }

    return (
        <div>
            <Header />

        </div>
    )
}

export default Home
