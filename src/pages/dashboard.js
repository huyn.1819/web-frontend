import {Nav} from "react-bootstrap";
import React from "react";

function Dashboard(props)
{
    return (
        <Nav justify variant="tabs" defaultActiveKey="/home">
            <Nav.Item>
                <Nav.Link href="/home">Home</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/login">Login</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/register">Sign up</Nav.Link>
            </Nav.Item>
        </Nav>
    )
}

export default Dashboard;