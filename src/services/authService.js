import API from "../config/axios";

const Login = (data) => {
    return API.post('login', data)
}

const getUser = () => {
    return API.get('users')
}

export { Login, getUser }
