1. convert class component to function component;
2. create env-example file and use .env file config api url dotenv;
3. add template: use material design
4. refactor structure folder.
5. context and custom context useAuth, login.
6. router: redirect route end config route to page. protected route.
7. redux: redux toolkit: user.
8. authorize use role permission.
9. custom interceptor axios.